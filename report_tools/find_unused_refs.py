import sys
import re

pattern = re.compile(r'\@\w+(\{[\w\-\:\/\.]+),')

if __name__ == "__main__":
    bib_path = sys.argv[1]
    tex_path = sys.argv[2]

    tex_content = ''

    with open(bib_path, 'r') as f:
        bib_content = f.read()
    
    with open(tex_path, 'r') as f:
        tex_content:str =f.read()
    
    references = re.findall(pattern, bib_content)
    print(references)
    print(len(references))

    for r in references:
        i = tex_content.find(r)
        if i == -1:
            print(r)