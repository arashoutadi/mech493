import sys
import cv2
import os

from typing import List


def split_rgb(exp_name, input_path, output_dir) -> List[str]:
    img = cv2.imread(input_path)
    b, g, r = cv2.split(img)

    cv2.imwrite(os.path.join(output_dir, "{0}-b.png".format(exp_name)), b)
    cv2.imwrite(os.path.join(output_dir, "{0}-g.png".format(exp_name)), g)
    cv2.imwrite(os.path.join(output_dir, "{0}-r.png".format(exp_name)), r)

    # For visualization
    cv2.imwrite(os.path.join(output_dir, "{0}-merge.png".format(exp_name)), img)

    return [
        os.path.join(output_dir, "{0}-b.png".format(exp_name)),
        os.path.join(output_dir, "{0}-g.png".format(exp_name)),
        os.path.join(output_dir, "{0}-r.png".format(exp_name)),
        os.path.join(output_dir, "{0}-merge.png".format(exp_name))
    ]


if __name__ == "__main__":
    exp_name = sys.argv[1]
    input_path = sys.argv[2]
    output_dir = sys.argv[3]

    split_rgb(exp_name, input_path, output_dir)
