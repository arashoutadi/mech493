from typing import List
import os
import pickle
import pprint

from common import Sample, Experiment, Centers
from utils import get_basename, get_centers
from split import split_rgb
from crop import crop_squares
import constants

experiments = [
    # Experiment("pre1", "Simple-Shapes", 50, 0, 4),
    # Experiment("exp1", "Small-Big", 50, 1, 2),
    # Experiment("exp2", "Polarized-Non_Polarized", 100, 1, 2),
    # Experiment("exp3", "Green-Blue", 50, 1, 2),
    # Experiment("exp4", "Green-Yellow", 50, 1, 2),
    # Experiment("exp5", "Eccentric", 50, 1, 2),
    Experiment("exp_r", "Random", 50, 1, 5)
]

all_samples: List[Sample] = []
class_counter = 0
for experiment in experiments:
    print("{0} Collection BEGIN: ".format(experiment.name))
    samples = []

    # Cairo
    if experiment.experiment_type == 0:
        segmentation_dir = os.path.join(constants.SEGMENTATION_DIR, "cairo")
        images = [
            os.path.join(os.getcwd(), de.path)
            for de in os.scandir(segmentation_dir)
            if not de.path.endswith(".gitkeep") and not de.path.endswith("tmerge.png")
        ]

        for i in range(len(images)):
            # Create the samples
            img_path = images[i]
            img_basename = get_basename(img_path)
            samples.append(
                Sample(
                    experiment.name,
                    int(img_basename[2]) + class_counter,
                    "",
                    [img_path],
                    "",
                )
            )
    else:
        # Get the generated source file
        file_path = os.path.join(
            constants.MATLAB_GENERATED_SIMUCELL, experiment.name + ".png"
        )
        src_paths = split_rgb(
            experiment.name, file_path, constants.SOURCE_GENERATED_SIMUCELL
        )

        centers = get_centers(
            os.path.join(constants.MATLAB_GENERATED_SIMUCELL, experiment.name + ".csv")
        )

        segmentation_dir = os.path.join(
            constants.SEGMENTATION_DIR, "simucell", experiment.name
        )
        try:
            os.mkdir(segmentation_dir)
            print("Creating new directory for: " + experiment.name)
        except FileExistsError:
            pass
        temp_samples = crop_squares(
            centers, experiment.name, src_paths, segmentation_dir, experiment.tile_size
        )

        samples = []
        for s in temp_samples:
            samples.append(
                Sample(
                    exp_name=s.exp_name,
                    population=s.population + class_counter,
                    xy=s.xy,
                    input_img_paths=s.input_img_paths,
                    vis_path=s.vis_path,
                )
            )

    all_samples.extend(samples)
    class_counter += experiment.num_populations
    print("{0} Collection END: ".format(experiment.name))

pickle_out = open(os.path.join(constants.CNN_DIR, 'train_r.pickle'), 'wb')
pickle.dump(all_samples, pickle_out)
pickle_out.close()
