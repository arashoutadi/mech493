import numpy as np
import cv2
from typing import Tuple, List, Dict, Optional
import os
import re
import csv

import common


# File operations
def deleteFiles(delete_dir: str):
    files_to_remove = [de for de in os.scandir(
        delete_dir) if os.path.isfile(de.path)]
    print("Removing {0} files from {1}".format(
        len(files_to_remove), delete_dir))
    for de in files_to_remove:
        if not de.name.endswith('.gitkeep'):
            os.remove(de.path)

def get_basename(path: str) -> str:
    return os.path.splitext(os.path.basename(path))[0]

# Super General Utils
def find(f, seq):
    """Return first item in sequence where f(item) == True."""
    for item in seq:
        if f(item):
            return item
    return None

# Segmentation Utils
def get_centers(centers_path: str) -> common.Centers:
    with open(centers_path, "r") as f:
        reader = csv.reader(f)
        return list(map(tuple, reader))

# Image Utils
def remove_black_background(img):
    tmp = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
    b, g, r = cv2.split(img)
    rgba = [b,g,r, alpha]
    return cv2.merge(rgba,4)
    

def show_image(img):
    cv2.imshow('image', img)
    k = cv2.waitKey(0)
    if k == 27:         # wait for ESC key to exit
        cv2.destroyAllWindows()


def square_stack(img1, img2, img3, img4):
    res1 = np.hstack((img1, img2))
    res2 = np.hstack((img3, img4))
    return np.vstack((res1, res2))


def get_hist_cdf(img: np.ndarray) -> np.ndarray:
    hist, bins = np.histogram(img.flatn(), 256, [0, 256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    return np.ma.filled(cdf_m, 0).astype('uint8')


def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def index_containing_substring(the_list, substring):
    for i, s in enumerate(the_list):
        if substring in s:
              return i
    return -1
    