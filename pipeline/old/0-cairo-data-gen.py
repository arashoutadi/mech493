import shutil
import math
import cairo
import numpy as np
import os
import random
from scipy.ndimage import gaussian_filter

from drawing_utils import Point, offset_point, rotate_n_degrees
import constants

WIDTH = 100
HEIGHT = WIDTH

CENTER_POINT = Point(WIDTH//2, HEIGHT//2)
TWO_PI = math.pi * 2

# POST PROCESSING
G_SIGMA = 1


def supershape_eval(
    m: float,
    radius: float,
    n1: float,
    n2: float,
    n3: float,
    phi: float,
) -> Point:
    a = 1
    b = 1

    t1 = math.cos(m * phi / 4) / a
    t1 = abs(t1)
    t1 = t1**n2

    t2 = math.sin(m * phi / 4) / b
    t2 = abs(t2)
    t2 = t2**n3

    r = (t1+t2) ** (1/n1)
    if (abs(r) == 0):
        return Point(x=0, y=0)
    else:
        r = radius / r
        return Point(x=r * math.cos(phi), y=r * math.sin(phi))


def intensity(_ctx: cairo.Context, num: float):
    _ctx.set_source_rgba(num, num, num, num)


def draw_supershape(_m: float, _radius: float, _ctx: cairo.Context):
    _ctx.save()
    phi_values = np.arange(0, TWO_PI, 0.01)
    p = offset_point(CENTER_POINT, supershape_eval(
        _m, _radius, 1, 1, 1, phi_values[0]))
    _ctx.move_to(p.x, p.y)
    # TODO: Can add more randomness here to make shape look more organic, use Perlin Noise
    for i in range(len(phi_values)):
        p = offset_point(
            CENTER_POINT,
            supershape_eval(m, _radius, 1, 1, 1, phi_values[i])
        )
        _ctx.line_to(p.x, p.y)
    _ctx.close_path()

    # TODO: Use perlin noise for intensity for more organic effect
    intensity(_ctx, .5)
    _ctx.set_line_width(2)
    _ctx.fill()


def post_process(surface: cairo.ImageSurface) -> cairo.ImageSurface:
    """Do some post-processing, which for now just means applying a gaussian filter
    to blur the image

    Arguments:
        surface {cairo.ImageSurface} -- [description]

    Returns:
        cairo.ImageSurface -- [description]
    """

    a: np.array = np.frombuffer(surface.get_data(), np.uint8)
    a = gaussian_filter(a, G_SIGMA)
    stride = cairo.ImageSurface.format_stride_for_width(cairo.FORMAT_A8, WIDTH)
    surface_out = cairo.ImageSurface.create_for_data(
        a, cairo.FORMAT_A8, WIDTH, HEIGHT, stride)
    return surface_out


rotations = {
    30: math.pi/6,
    60: math.pi/3,
    90: math.pi/2,
    120: 2*math.pi/3,
    150: 5*math.pi/6,
    180: math.pi,
}

print('Deleting files inside: {0}'.format(constants.SOURCE_GENERATED_SIMPLE))
for file in os.listdir(constants.SOURCE_GENERATED_SIMPLE):
    file_path = os.path.join(constants.SOURCE_GENERATED_SIMPLE, file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception as e:
        print(e)

for i in range(100):
    with cairo.ImageSurface(cairo.FORMAT_A8, WIDTH, HEIGHT) as surface:
        # TODO: Eventually make this more sophisticated
        m = i % 4+1
        radius = 20 + random.randint(10, 20)

        ctx = cairo.Context(surface)
        rotation_name = random.choice(list(rotations.keys()))
        rotate_n_degrees(ctx, CENTER_POINT, rotations[rotation_name])
        draw_supershape(m, radius, ctx)

        name = 'p-{0}_r-{1}_rot-{2}.png'.format(m, radius, rotation_name)
        save_path = os.path.join(constants.SOURCE_GENERATED_SIMPLE, name)
        # surface = post_process(surface)
        surface.write_to_png(save_path)
