from typing import NamedTuple

import math
import random
import numpy as np
import cairo

from PIL import Image


class Point(NamedTuple):
    x: float
    y: float


def organic_arc(ctx: cairo.Context, r: int, lower_angle: float, upper_angle: float):
    raise NotImplementedError

def offset_point(o: Point, p: Point) -> Point:
    return Point(p.x + o.x, p.y + o.y)


def random_point_within_circle(R: int) -> Point:
    r = R * math.sqrt(random.random())
    theta = random.random() * 2 * math.pi
    x = r * math.cos(theta)
    y = r * math.sin(theta)
    return Point(int(x), int(y))


def point_on_radius(r: float, theta: float) -> Point:
    x = r * math.cos(theta)
    y = r * math.sin(theta)
    return Point(int(x), int(y))

def rotate_n_degrees(_ctx: cairo.Context, _rotation_point: Point, _radians: float):
    _ctx.translate(_rotation_point.x, _rotation_point.y)
    _ctx.rotate(_radians)
    _ctx.translate(-_rotation_point.x, -_rotation_point.y)