from PIL import Image
import numpy as np
import constants
import os
from utils import find
import ntpath

files_8 = [x.path for x in os.scandir(constants.MASKED_DIR_8)]
layer0 = Image.open(find(lambda p: 'CY5' in p, files_8)).getdata()
layer1 = Image.open(find(lambda p: 'EGFP' in p, files_8)).getdata()
layer2 = Image.open(find(lambda p: 'DAPI' in p, files_8)).getdata()

w, h = layer0.size
ima = Image.new('RGB', (w, h))
data = zip(layer0.convert('L'), layer1.convert('L'), layer2.convert('L'))
ima.putdata(list(data))
ima.save(os.path.join(constants.MASKED_DIR_8, 'color_merged.png'))
