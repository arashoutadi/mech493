import time
import os
import numpy as np

import pickle

from typing import List

from cnn import FeedForwardCNN
from common import Sample, EnrichedSample


def cnn_extract( _cnn: FeedForwardCNN, _samples: List[Sample], exp_name: str, output_dir: str) -> List[EnrichedSample]:
    """ 
    STEP 0.2:
    Initialize CNN 
    Pre-Allocate Feature Vector Array
    """

    m = len(_samples)
    feature_vectors = [np.empty((1, 1))] * m
    names = [''] * m
    paths = [''] * m

    """
    STEP 1:
    Feed each GroupedImage (stack of images as nd.array) into CNN and flatten resulting output
    """

    start = time.time()
    _enriched_samples = []
    for i, sample in enumerate(_samples):
        # print('Getting prediction for images: {0}-{1} x {2}'.format(sample.population, sample.xy, len(sample.input_img_paths)))
        predictions = _cnn.feed_forward(sample.input_img_paths)
        feature_vector = predictions.ravel()
        feature_vectors[i] = feature_vector

        es = EnrichedSample(feature_vector=feature_vector, **sample._asdict())
        _enriched_samples.append(es)

    print("Feature Vector Shape: {0}".format(feature_vectors[0].shape))
    end = time.time()

    """
    STEP 1.5:
    Save the feature vectors to a file for fast retrieval
    """

    print('Elapsed time for computing feature vectors: {0}'.format(
        end - start))

    start = time.time()
    pickle_out = open(os.path.join(output_dir, exp_name + '_enriched_samples.pickle'), "wb")
    pickle.dump(_enriched_samples, pickle_out)
    pickle_out.close()

    end = time.time()

    print('Elapsed time for saving samples to {0}: {1}'.format(
        pickle_out.name, end - start))
    return _enriched_samples
