import sys
import subprocess
import os
import re
import cv2
import csv
from typing import List, Tuple, Optional, NamedTuple
import time
import numpy as np
import cnn
import pprint

# Local Files
from split import split_rgb
from common import Centers, Sample, EnrichedSample, ClusterSample, Experiment
from crop import crop_squares
from extract import cnn_extract
from utils import remove_black_background, get_basename, get_centers
import constants
import cluster


def calculate_misclass_rate(clustered_samples: List[ClusterSample]) -> float:
    misclass_count = 0

    for cs in clustered_samples:
        misclass_count += 1 if cs.population != cs.cluster else 0
    misclass_rate = misclass_count / len(clustered_samples)
    return misclass_rate if misclass_rate < 0.5 else 1 - misclass_rate



if __name__ == "__main__":
    if len(sys.argv) > 1:
        print(sys.argv)
        model_path = sys.argv[1]
        ff_cnn = cnn.FeedForwardCNN(model_path, 2)
    else:
        ff_cnn = cnn.FeedForwardCNN(None, 1)
        
    experiments = [
        # Experiment("pre1", "Simple-Shapes", 50, 0, 4),
        Experiment("exp1", "Small-Big", 50, 1, 2),
        Experiment("exp2", "Polarized-Non_Polarized", 100, 1, 2),
        Experiment("exp3", "Green-Blue", 50, 1, 2),
        Experiment("exp4", "Green-Yellow", 50, 1, 2),
        # Experiment('exp5', 'Eccentric', 50, 1, 2),
        Experiment('exp6', 'Eccentric2', 50, 1, 2),
        # Experiment('exp_r', 'Random', 50, 1, 5)
    ]   

    results = []
    for experiment in experiments:
        print("{0} Analysis BEGIN: ".format(experiment.name))
        samples = []  # Assigned in branches
        mc_rates = {}
        # Cairo
        if experiment.experiment_type == 0:
            segmentation_dir = os.path.join(constants.SEGMENTATION_DIR, "cairo")
            images = [
                os.path.join(os.getcwd(),de.path)
                for de in os.scandir(segmentation_dir)
                if not de.path.endswith(".gitkeep") and not de.path.endswith("tmerge.png")
            ]

            for i in range(len(images)):
                # Make visualization image
                img_path = images[i]
                img = cv2.imread(img_path)
                vis_img = remove_black_background(img)
                img_basename = get_basename(img_path)
                vis_path = os.path.join(segmentation_dir, img_basename + "-tmerge.png")
                cv2.imwrite(vis_path, vis_img)
                # Create the samples
                samples.append(
                    Sample(experiment.name, int(img_basename[2]), "na", [img_path], vis_path)
                )

            enriched_samples = cnn_extract(ff_cnn, samples, experiment.name, constants.CNN_DIR)
            # Clustering with different algorithms
            clustered_samples = cluster.hac(
                enriched_samples, experiment, constants.CLUSTER_DIR
            )
            clustered_samples = cluster.kmeans(
                enriched_samples, experiment, constants.CLUSTER_DIR
            )

        # Simucell
        else:
            # Get the generated source file
            file_path = os.path.join(
                constants.MATLAB_GENERATED_SIMUCELL, experiment.name + ".png"
            )
            src_paths = split_rgb(
                experiment.name, file_path, constants.SOURCE_GENERATED_SIMUCELL
            )

            centers = get_centers(
                os.path.join(constants.MATLAB_GENERATED_SIMUCELL, experiment.name + ".csv")
            )

            segmentation_dir = os.path.join(
                constants.SEGMENTATION_DIR, "simucell", experiment.name
            )
            try:
                os.mkdir(segmentation_dir)
                print("Creating new directory for: " + experiment.name)
            except FileExistsError:
                pass
            samples = crop_squares(
                centers, experiment.name, src_paths, segmentation_dir, experiment.tile_size
            )

            enriched_samples = cnn_extract(ff_cnn, samples, experiment.name, constants.CNN_DIR)
            # Clustering with different algorithms
            clustered_samples = cluster.hac(
                enriched_samples, experiment, constants.CLUSTER_DIR
            )
            misclass_rate = calculate_misclass_rate(clustered_samples)
            mc_rates["hac"] = misclass_rate

            clustered_samples = cluster.kmeans(
                enriched_samples, experiment, constants.CLUSTER_DIR
            )
            misclass_rate = calculate_misclass_rate(clustered_samples)
            mc_rates["kmeans"] = misclass_rate

        results.append((experiment.name, experiment.pretty_name, mc_rates))

        print("{0} Analysis END: ".format(experiment.name))

    pprint.pprint(results)

## Results
### Remove 1 layer
# [('exp1', 'Small-Big', {'hac': 0.0, 'kmeans': 0.0}),
#  ('exp2', 'Polarized-Non_Polarized', {'hac': 0.0, 'kmeans': 0.46}),
#  ('exp3', 'Green-Blue', {'hac': 0.020000000000000018, 'kmeans': 0.36}),
#  ('exp4', 'Green-Yellow', {'hac': 0.48, 'kmeans': 0.38})]
### Remove 2 layers
# [('exp1', 'Small-Big', {'hac': 0.32, 'kmeans': 0.28}),
#  ('exp2', 'Polarized-Non_Polarized', {'hac': 0.24, 'kmeans': 0.38}),
#  ('exp3', 'Green-Blue', {'hac': 0.28, 'kmeans': 0.0}),
#  ('exp4', 'Green-Yellow', {'hac': 0.22, 'kmeans': 0.24})]

