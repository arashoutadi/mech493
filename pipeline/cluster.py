import numpy as np
import os
import sys
import constants
import pickle

from typing import List, NamedTuple

from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import fcluster

from sklearn.cluster import KMeans

from common import EnrichedSample, ClusterSample, Experiment

def hac(samples: List[EnrichedSample], exp: Experiment,  output_dir: str) -> List[ClusterSample]:
    X = np.asarray([sample.feature_vector for sample in samples])
    Z = linkage(X, 'ward')

    clusters = fcluster(Z, exp.num_populations, criterion='maxclust')
    clustered_samples = []

    for i in range(len(clusters)):
        s = samples[i]
        clustered_samples.append(ClusterSample(
            cluster=clusters[i], **s._asdict()))
    
    pickle_out = open(os.path.join(output_dir, exp.name + '_cluster.hac.pickle'), "wb")
    pickle.dump(clustered_samples, pickle_out)
    pickle_out.close()

    return clustered_samples

def kmeans(samples: List[EnrichedSample], exp: Experiment,  output_dir: str) -> List[ClusterSample]:
    X = np.asarray([sample.feature_vector for sample in samples])

    kmeans = KMeans(n_clusters=exp.num_populations, random_state=0).fit(X)

    clusters = kmeans.labels_
    clustered_samples = []

    for i in range(len(clusters)):
        s = samples[i]
        clustered_samples.append(ClusterSample(
                cluster=clusters[i]+1, **s._asdict()))

    pickle_out = open(os.path.join(output_dir, exp.name + '_cluster.kmeans.pickle'), "wb")
    pickle.dump(clustered_samples, pickle_out)
    pickle_out.close()

    return clustered_samples