import sys
import os
from typing import List, Tuple
import cv2
import numpy as np
import h5py
import csv

from common import Sample, Centers
from utils import deleteFiles, remove_black_background



def crop_square_around_center(img, cx: int, cy: int, tile_side_length: int):
    w = tile_side_length
    h = tile_side_length
    return img[cy-w//2:cy+w//2, cx-h//2:cx+h//2]

def crop_squares(_centers: Centers, _exp_name: str, _source_paths: List[str], _output_dir: str, tile_length=100) -> List[Sample]:
    deleteFiles(_output_dir)
    samples = []
    for center in _centers:
        cx = int(float(center[0]))
        cy = int(float(center[1]))
        p = int(center[2])
        save_paths = []

        # Crop the RGB channels
        for i in range(3):
            src_img_path = _source_paths[i]
            img = cv2.imread(src_img_path)
            cropped_img = crop_square_around_center(img, cx, cy, tile_length)
            grayscale_img = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY)
            channel = src_img_path[-5]
            save_name = '{0}-p_{1}-x_{2}-y_{3}-c_{4}.png'.format(_exp_name, p, cx, cy, channel)
            save_path = os.path.join(_output_dir, save_name)
            save_paths.append(save_path)
            cv2.imwrite(save_path, grayscale_img)

        # Last source path is the merged
        img = cv2.imread(_source_paths[-1])
        cropped_img = crop_square_around_center(img, cx, cy, tile_length)
        # Remove the black background for better visualizations
        dst = remove_black_background(cropped_img)

        save_name = '{0}-p_{1}-x_{2}-y_{3}-c_{4}.png'.format(_exp_name, p, cx, cy, 'merge_transparent')
        vis_path = os.path.join(_output_dir, save_name)
        cv2.imwrite(vis_path, dst)
        
        # NOTE: Last image is the merged image
        sample = Sample(exp_name=_exp_name, population=p, xy='x_{0}-y_{1}'.format(cx, cy), input_img_paths=save_paths, vis_path=vis_path)
        samples.append(sample)
    
    return samples

if __name__ == "__main__":
    pass    