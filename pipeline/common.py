import numpy as np

from typing import List, Tuple, NamedTuple

# (x, y, population)
Centers = List[Tuple[int, int, int]]

class Experiment(NamedTuple):
    name: str
    pretty_name: str
    tile_size: int
    experiment_type: int
    num_populations: int

class Sample(NamedTuple):
    exp_name: str
    population: int
    xy: str
    input_img_paths: List[str]
    vis_path: str

# Cannot subclass NamedTuples because they are immutable
class EnrichedSample(NamedTuple):
    exp_name: str
    population: str
    xy: str
    input_img_paths: List[str]
    vis_path: str
    feature_vector: np.ndarray

class ClusterSample(NamedTuple):
    exp_name: str
    population: str
    xy: str
    input_img_paths: List[str]
    vis_path: str
    feature_vector: np.ndarray
    cluster: int
