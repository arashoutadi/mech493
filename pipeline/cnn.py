from typing import Union, List
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import cv2
import os
from enum import Enum
import constants

from keras import applications, Sequential
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras.applications.imagenet_utils import decode_predictions
from keras.utils import plot_model
from keras.models import load_model

# TODO: Print CNN archecticture
# import pydot
# pydot.find_graphviz = lambda: True

def gray2rgb_inception(img: Union[str, np.ndarray]) -> np.ndarray:
    """Read a grey-scale image and output a scaled RGB 299x299 pixel image

    Arguments:
        path

    Returns:
        [type] -- [description]
    """
    if type(img) == str:
        x = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
    else:
        x = img
    z = np.stack((x, x, x), axis=2)
    z = applications.inception_v3.preprocess_input(z)
    return cv2.resize(z, dsize=(299, 299))


class FeedForwardCNN():
    def __init__(self, saved_model_path = None, remove_n_layers=1):
        """Initializes the inception CNN"""

        IN_SHAPE = (299, 299, 3) # image dimensions and RGB channels
        if saved_model_path is not None:
            model = load_model(saved_model_path)
            for i in range(remove_n_layers):
                model.layers.pop()
            model.outputs = [model.layers[-1].output]
            model.layers[-1].outbound_nodes = []
            self.model = model

        else:
            if remove_n_layers == 1:
                inception_model = applications.InceptionV3(weights="imagenet", pooling='max', include_top=False, input_shape=IN_SHAPE)
                self.model = inception_model
            elif remove_n_layers > 0:
                inception_model = applications.InceptionV3(weights="imagenet", pooling='max')
                for i in range(remove_n_layers):
                    inception_model.layers.pop()

                inception_model.outputs = [model.layers[-1].output]
                inception_model.layers[-1].outbound_nodes = []
                self.model = inception_model
            else:
                self.model = applications.InceptionV3(weights="imagenet")

        print('Last 5 layers:')
        for l in self.model.layers[-5:]:
            print(l.name, l.output_shape)

        # plot_model(self.model, show_shapes=True, to_file='{0}.pdf'.format('modified_inception'))

    def feed_forward(self, grouped_images: Union[List[str], np.ndarray], experiment_type = 1) -> np.ndarray:
        """Function that takes a batch of images returns the output of the CNN for each image

        Arguments:
            batch_imgs {np.ndarray} -- [description]

        Returns:
            np.ndarray -- [description]
        """
        if type(grouped_images[0]) == str:
            batch_imgs = np.asarray([gray2rgb_inception(p)
                                     for p in grouped_images])
        else:
            batch_imgs = grouped_images
        return self.model.predict(batch_imgs)


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


def test_run(filepath: str):
    original = load_img(filepath, target_size=(299, 299, 3))
    np_img = img_to_array(original)
    print(np_img.shape)
    gray = rgb2gray(np_img)
    model_input = gray2rgb_inception(gray)
    # plt.imshow(numpy_image, interpolation='nearest')
    # plt.show()
    ff_cnn = FeedForwardCNN()
    predictions = ff_cnn.feed_forward(np.asarray([model_input]))

    # convert the probabilities to class labels
    # We will get top 5 predictions which is the default
    labels = decode_predictions(predictions)
    print(labels)


if __name__ == "__main__":
    filepath = os.path.join('3-cnn', 'test_images', 'cat.jpg')
    test_run(filepath)
