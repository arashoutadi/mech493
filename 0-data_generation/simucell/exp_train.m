subpop=cell(0); 

n = 5;
for i = 1:n
    default_c = create_default_cell();
    default_c.cyto.random = 0.5;
    default_c.nuc.random = 0.5;
    default_c.cyto.ecc = rand();
    default_c.nuc.ecc = rand();
    r = rand();
    default_c.nuc.radius = 10*r + 2*r;
    default_c.nuc.cyto = 15*r + 10*r;
    
    subpop{i}=create_subpop(default_c);
end

simucell_data.population_fractions=randfixedsum(n,1,1,1/n,.5); 
simucell_data.number_of_cells=60;
simucell_data.simucell_image_size=[2000,2000];
simucell_data.subpopulations=subpop;
