subpop=cell(0); 
default_c = create_default_cell();
default_c.cyto.ecc = 0.9;
default_c.nuc.ecc = 0.05;    
default_c.cyto.random = 0.2;
default_c.nuc.random = 0.2;
default_c.nuc.radius = 14;
default_c.nuc.cyto = 35;
subpop{1}=create_subpop(default_c);

default_c.cyto.ecc = 0.2;
default_c.nuc.ecc = 0.95;
default_c.nuc.radius = 23;
default_c.nuc.cyto = 27;
subpop{2}=create_subpop(default_c);

simucell_data.population_fractions=[0.5 0.5]; 
simucell_data.number_of_cells=60;
simucell_data.simucell_image_size=[2000,2000];
simucell_data.subpopulations=subpop;