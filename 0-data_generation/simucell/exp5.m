subpop=cell(0); 
default_c = create_default_cell();
default_c.cyto.ecc = 0.8;
default_c.nuc.ecc = 0.1;    
default_c.cyto.random = 0.08;
default_c.nuc.random = 0.08;
default_c.nuc.radius = 10;
default_c.nuc.cyto = 40;
subpop{1}=create_subpop(default_c);

default_c.cyto.ecc = 0.1;
default_c.nuc.ecc = 0.8;
default_c.nuc.radius = 20;
default_c.nuc.cyto = 25;
subpop{2}=create_subpop(default_c);

simucell_data.population_fractions=[0.5 0.5]; 
simucell_data.number_of_cells=60;
simucell_data.simucell_image_size=[2000,2000];
simucell_data.subpopulations=subpop;