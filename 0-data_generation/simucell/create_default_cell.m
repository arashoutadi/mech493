function [default_cell] = create_default_cell()
default_cell = struct();
default_cell.cyto = struct('random', 0.05, 'ecc', 0.05, 'radius', 20);
default_cell.nuc = struct('random', 0.05, 'ecc', 0.05, 'radius', 10);
default_cell.colors = struct('myosin', 'Green', 'actin', 'Red', 'density', 'Blue');
end

