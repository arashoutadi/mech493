subpop=cell(0); 
default_c = create_default_cell();
subpop{1}=create_subpop(default_c);

default_c.nuc.radius = 5;
default_c.cyto.radius = 10;
subpop{2}=create_subpop(default_c);

simucell_data.population_fractions=[0.5 0.5]; 
simucell_data.number_of_cells=50;
simucell_data.simucell_image_size=[2000,2000];
simucell_data.subpopulations=subpop;


