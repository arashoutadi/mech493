function [subpop] = create_subpop(cs)
    subpop = Subpopulation();
    subpop.placement=Random_Placement();
    set(subpop.placement,'boundary',100);  
    subpop.compositing=Default_Compositing();
    set(subpop.compositing,'container_weight',0.3);
    %% Cytoplasm
    add_object(subpop,'cytoplasm');
    subpop.objects.cytoplasm.model=Cytoplasm_Model;
    set(subpop.objects.cytoplasm.model,'radius', cs.cyto.radius); 
    set(subpop.objects.cytoplasm.model,'eccentricity',cs.cyto.ecc); 
    set(subpop.objects.cytoplasm.model,'randomness', cs.cyto.random);
    %% Nucleus
    add_object(subpop,'nucleus');
    subpop.objects.nucleus.model=Centered_Nucleus_Model;
    set(subpop.objects.nucleus.model,'radius',cs.nuc.radius); 
    set(subpop.objects.nucleus.model,'eccentricity',cs.nuc.ecc); 
    set(subpop.objects.nucleus.model,'randomness', cs.nuc.random); 
    set(subpop.objects.nucleus.model,'centered_around',subpop.objects.cytoplasm); 
    %% Actin
    add_marker(subpop,'Actin',cs.colors.actin);
    op=Constant_Marker_Level();
    set(op,'mean_level',0.7); 
    set(op,'sd_level',0.5); 
    subpop.markers.Actin.cytoplasm.AddOperation(op); 
    op=Distance_To_Shape_Marker_Gradient();
    set(op,'falloff_radius', 10); 
    set(op,'falloff_type','Gaussian'); 
    set(op,'increasing_or_decreasing','Decreasing'); 
    set(op,'distance_to',subpop.objects.nucleus); 
    subpop.markers.Actin.cytoplasm.AddOperation(op);
    
    op=Perlin_Texture();
    set(op,'amplitude',0.3); 
    set(op,'length_scale',5); 
    set(op,'frequency_falloff',0.5); 
    set(op,'noise_type','Turbulent'); 
    subpop.markers.Actin.cytoplasm.AddOperation(op);
    op=Turbulent_Texture();
    set(op,'max_displacement',7); 
    set(op,'length_scale',5); 
    set(op,'frequency_falloff',0.8); 
    subpop.markers.Actin.cytoplasm.AddOperation(op);
    %% Myosin
    add_marker(subpop,'Myosin',cs.colors.myosin);
    op=Constant_Marker_Level();
    set(op,'mean_level',0.9);
    set(op,'sd_level',0.1);
    subpop.markers.Myosin.cytoplasm.AddOperation(op);
    op=Distance_To_Edge_Marker_Gradient();
    set(op,'falloff_radius',10); 
    set(op,'falloff_type','Gaussian'); 
    set(op,'increasing_or_decreasing','Decreasing'); 
    subpop.markers.Myosin.cytoplasm.AddOperation(op);
    op=Turbulent_Texture();
    set(op,'max_displacement',5);
    set(op,'length_scale',4);
    set(op,'frequency_falloff',0.7);
    subpop.markers.Myosin.cytoplasm.AddOperation(op);
    %% Density Marker
    add_marker(subpop,'Density_Marker','Blue');
    op=Cell_Density_Dependant_Marker_Level();
    set(op,'max_level',2); 
    set(op,'falloff_radius',20); 
    set(op,'falloff_type','Gaussian'); 
    set(op,'increasing_or_decreasing','Decreasing'); 
    subpop.markers.Density_Marker.nucleus.AddOperation(op);
    
    op=Perlin_Texture();
    set(op,'amplitude',0.7);
    set(op,'length_scale',5);
    set(op,'frequency_falloff',0.8);
    subpop.markers.Density_Marker.nucleus.AddOperation(op);
  
    op=Turbulent_Texture();
    set(op,'max_displacement',5);
    set(op,'length_scale',4);
    set(op,'frequency_falloff',0.7);
    subpop.markers.Density_Marker.nucleus.AddOperation(op);
    op=Constant_Dependant_Marker_Level(); 
    set(op,'slope',-1.2); 
    set(op,'intercept',0.6); 
    set(op,'marker',subpop.markers.Density_Marker.nucleus); 
    set(op,'region',subpop.objects.nucleus); 
    subpop.markers.Density_Marker.cytoplasm.AddOperation(op);
    
    op=Perlin_Texture();
    set(op,'amplitude',0.4);
    set(op,'length_scale',3);
    set(op,'frequency_falloff',0.8);
    set(op,'noise_type','Turbulent');
    subpop.markers.Density_Marker.cytoplasm.AddOperation(op);
    
    %% Artifacts
    op=Out_Of_Focus_Cells(); 
    set(op,'fraction_blurred',1); 
    set(op,'blur_radius',2);
    subpop.add_cell_artifact(op);
    op=Out_Of_Focus_Cells();
    set(op,'fraction_blurred',0.1); 
    set(op,'blur_radius',4); 
    subpop.add_cell_artifact(op);
end

