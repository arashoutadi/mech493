%% Run Script
startSimucell
for i = 1:10
    train_name = sprintf('%s%d', 'train', i);
    disp("Generating: " + train_name);
    r = simucell('exp_train');
    close all
    centroids = get_centroids(r);
    f = fullfile('generated',train_name);
    csvwrite(sprintf('%s%s', f, '.csv'), centroids);
    imwrite(r.RGB_image , sprintf('%s%s', f, '.png'), 'png');
end
