%% Run Script
startSimucell
for i = 6:6
    exp_name = sprintf('%s%d', 'exp', i);
    disp("Generating: " + exp_name);
    r = simucell(exp_name);
    close all
    centroids = get_centroids(r);
    f = fullfile('generated',exp_name);
    csvwrite(sprintf('%s%s', f, '.csv'), centroids);
    imwrite(r.RGB_image , sprintf('%s%s', f, '.png'), 'png');
end

exp_name = 'exp_r';
disp("Generating: " + exp_name);
r = simucell(exp_name);
close all
centroids = get_centroids(r);
f = fullfile('generated',exp_name);
csvwrite(sprintf('%s%s', f, '.csv'), centroids);
imwrite(r.RGB_image , sprintf('%s%s', f, '.png'), 'png');