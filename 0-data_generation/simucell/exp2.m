subpop=cell(0); 
subpop{1}=Subpopulation();

subpop{1}.placement=Random_Placement();
set(subpop{1}.placement,'boundary',100);  % the boundary is the number of pixels around the edge of the image where no cells are placed

subpop{1}.compositing=Default_Compositing();
set(subpop{1}.compositing,'container_weight',0.3);


add_object(subpop{1},'cytoplasm');
subpop{1}.objects.cytoplasm.model=Cytoplasm_Model;
set(subpop{1}.objects.cytoplasm.model,'radius',40); %cell radius in pixels
set(subpop{1}.objects.cytoplasm.model,'eccentricity',0.01); % Gives a nearly circular cell
set(subpop{1}.objects.cytoplasm.model,'randomness',0.05); %0 is no noise, and 1 is all noise, so this cell is fairly smooth

add_object(subpop{1},'nucleus');
subpop{1}.objects.nucleus.model=Centered_Nucleus_Model;
set(subpop{1}.objects.nucleus.model,'radius',15); %nuclear radius in pixels
set(subpop{1}.objects.nucleus.model,'eccentricity',0.6); % nuclei are typically elliptical
set(subpop{1}.objects.nucleus.model,'randomness',0.2); % make the nuclear shape a little noisy
set(subpop{1}.objects.nucleus.model,'centered_around',subpop{1}.objects.cytoplasm); % the nucleus is dran at the center of the cytoplasm



add_marker(subpop{1},'Actin','Green');
op=Constant_Marker_Level();
set(op,'mean_level',0.7); % The marker level for a cell is sampled from a normal distribution with this mean
set(op,'sd_level',0.1); % and this standard deviation
subpop{1}.markers.Actin.cytoplasm.AddOperation(op); % Once the operation is defined, we add it to the queue

op=Distance_To_Shape_Marker_Gradient();
set(op,'falloff_radius',10); % The number of pixels over which intensity falls by 1/e
set(op,'falloff_type','Gaussian'); % the intensity fall off functional form
set(op,'increasing_or_decreasing','Decreasing'); % whether intensity increases or decreases based on the distance
set(op,'distance_to',subpop{1}.objects.nucleus); % distance is measure wrt the nucleus
subpop{1}.markers.Actin.cytoplasm.AddOperation(op);

op=Perlin_Texture();
set(op,'amplitude',0.3); % the amplitude of the noise (0 is not noise, and you probably don't want to go much beyond 1, since intensities are in the [0-1] range)
set(op,'length_scale',5); % This descibes the length scale over which intensity varies. 2 is low wave-length (coarse variation) and 6 is very fine high frequency (fine variation)
set(op,'frequency_falloff',0.5); % Each higher frequency component amplitude is smaller by this scaling factor
set(op,'noise_type','Turbulent'); % The type of noise. Turbulent causes sharper transitions
subpop{1}.markers.Actin.cytoplasm.AddOperation(op);
op=Turbulent_Texture();
set(op,'max_displacement',7); % This is the max distance in pixels that a pixel can be moved
set(op,'length_scale',5); % same length scale defiition as Perlin above
set(op,'frequency_falloff',0.8); % same as Perlin above
subpop{1}.markers.Actin.cytoplasm.AddOperation(op);

add_marker(subpop{1},'Myosin','Red');
op=Constant_Marker_Level();
set(op,'mean_level',0.9);
set(op,'sd_level',0.1);
subpop{1}.markers.Myosin.cytoplasm.AddOperation(op);

op=Distance_To_Edge_Marker_Gradient();
set(op,'falloff_radius',10); %pixels over which intensity falls off by 1/e 
set(op,'falloff_type','Gaussian'); %the intensity fall off functional form
set(op,'increasing_or_decreasing','Decreasing'); %Intensity decreases with distance to the edge
subpop{1}.markers.Myosin.cytoplasm.AddOperation(op);
op=Turbulent_Texture();
set(op,'max_displacement',5);
set(op,'length_scale',4);
set(op,'frequency_falloff',0.7);
subpop{1}.markers.Myosin.cytoplasm.AddOperation(op);


 add_marker(subpop{1},'Density_Marker','Blue');


op=Cell_Density_Dependant_Marker_Level();
set(op,'max_level',2); % Max level (theoretically) possible, if cells were on top of each other (increasing this increases brightness across all cells)
set(op,'falloff_radius',200); % The distance (in pixels) that the nearest cell must be on average for intensity to fall off by a factor of 1/e
set(op,'falloff_type','Gaussian'); % Functional form of dependance of intensity on the average distance to nearest cell
set(op,'increasing_or_decreasing','Decreasing'); % Does intensity increase or decrease with local cell density?
subpop{1}.markers.Density_Marker.nucleus.AddOperation(op);

op=Perlin_Texture();
set(op,'amplitude',0.7);
set(op,'length_scale',5);
set(op,'frequency_falloff',0.8);
% set(op,'noise_type','Turbulent');
subpop{1}.markers.Density_Marker.nucleus.AddOperation(op);
% Throw in some turbulence
% Turbulent Texture
op=Turbulent_Texture();
set(op,'max_displacement',5);
set(op,'length_scale',4);
set(op,'frequency_falloff',0.7);
subpop{1}.markers.Density_Marker.nucleus.AddOperation(op);

% Set the marker level in the cytoplasm to a constant, with this constant
% varying inversely with the mean level of the marker in the nucleus
op=Constant_Dependant_Marker_Level(); % If x_(m,r) is the mean level in of marker m in region r, then 
% this plugin sets the intensity of chosen marker in chosen region to be:
% slope*x_(m,r) + intercept
set(op,'slope',-1.2); % slope in the equation above, negative sign means inverse relation
set(op,'intercept',0.6); % intercept in equation above. This is the level the marker will have if the other marker x_(m,r) is zero
set(op,'marker',subpop{1}.markers.Density_Marker.nucleus); % The other marker i.e. m on which this marker depends
set(op,'region',subpop{1}.objects.nucleus); % The region on which m is calculated i.e. r
subpop{1}.markers.Density_Marker.cytoplasm.AddOperation(op);
% Throw in some noise
%Perlin Texture
op=Perlin_Texture();
set(op,'amplitude',0.4);
set(op,'length_scale',3);
set(op,'frequency_falloff',0.8);
set(op,'noise_type','Turbulent');
subpop{1}.markers.Density_Marker.cytoplasm.AddOperation(op);


op=Out_Of_Focus_Cells(); % Makes a specified fracton of cells blurred, to mimic an out of focal plane effect
set(op,'fraction_blurred',1); % All cells blurred
set(op,'blur_radius',2);% blur radius in pixels. 1 is pretty small, chosen to produce a very slight smoothing effect on all cells
subpop{1}.add_cell_artifact(op);
op=Out_Of_Focus_Cells();
set(op,'fraction_blurred',0.1); % For a few cells (10%)
set(op,'blur_radius',4); % we choose to make them more seriously out of focus
subpop{1}.add_cell_artifact(op);



%Define Subpopulation 1
subpop{2}=Subpopulation();

%Set the Model Placement
subpop{2}.placement=Random_Placement();
set(subpop{2}.placement,'boundary',100);

%Set the Composite Type
subpop{2}.compositing=Default_Compositing();
set(subpop{2}.compositing,'container_weight',0.3);

%% SP2 Shape
%Set the Object Shape
%Object 1
add_object(subpop{2},'cytoplasm');
subpop{2}.objects.cytoplasm.model=Cytoplasm_Model;
set(subpop{2}.objects.cytoplasm.model,'radius',60);
set(subpop{2}.objects.cytoplasm.model,'eccentricity',0.6); % Note this subpopulation is far more eccentric than the first
set(subpop{2}.objects.cytoplasm.model,'randomness',0.2);

add_object(subpop{2},'nucleus');
subpop{2}.objects.nucleus.model=Centered_Nucleus_Model;
set(subpop{2}.objects.nucleus.model,'radius',10);
set(subpop{2}.objects.nucleus.model,'eccentricity',0.6);
set(subpop{2}.objects.nucleus.model,'randomness',0.2);
set(subpop{2}.objects.nucleus.model,'centered_around',subpop{2}.objects.cytoplasm);


%% SP2 Markers
% Like for the first subpopulation we have the Red marker at the edge,
% however we add an angular dependance so that it is confined to one side
% of the cell and appears polarized,
%Marker 1
add_marker(subpop{2},'Myosin','Red');
op=Constant_Marker_Level();
set(op,'mean_level',0.8);
set(op,'sd_level',0.1);
subpop{2}.markers.Myosin.cytoplasm.AddOperation(op);
%Add Angular gradient 
op=Angular_Marker_Gradient();
% the angle made by every pixel (measured with respect to a point
% determined by the 'center' parameter) with a randomly chosen direction is
% calculated. The intensity at the pixels falls off with this angle, with
% the functional form specified in 'falloff_type' falling to 1/e at
% 'angular_width'
set(op,'center','Furthest From Edge');
set(op,'angular_width',30);
set(op,'falloff_type','Exponential');
set(op,'min_multiplier',0);
subpop{2}.markers.Myosin.cytoplasm.AddOperation(op);
%Add Radial Gradient
op=Distance_To_Edge_Marker_Gradient();
set(op,'falloff_radius',15);
set(op,'falloff_type','Gaussian');
set(op,'increasing_or_decreasing','Decreasing');
subpop{2}.markers.Myosin.cytoplasm.AddOperation(op);
%Perlin Texture
op=Perlin_Texture();
set(op,'amplitude',0.5);
set(op,'length_scale',5);
set(op,'frequency_falloff',0.8);
set(op,'noise_type','Turbulent');
subpop{2}.markers.Myosin.cytoplasm.AddOperation(op);
% Turbulent Texture
op=Turbulent_Texture();
set(op,'max_displacement',5);
set(op,'length_scale',4);
set(op,'frequency_falloff',0.8);
subpop{2}.markers.Myosin.cytoplasm.AddOperation(op);



%Marker 2
add_marker(subpop{2},'Actin','Green');
% In this case we have the green marker depend on the the red one, and
% avoid it. This is done at a pixel level. If I is the intensity of the red
% marker at a pixel then the intensity of the green marker is:
% slope*I+intercept (where slope and intercept are specified below). A
% negative value of the slope implies supression. 
op=Locally_Dependant_Marker_Level();
set(op,'slope',-100); % The red marker strongly supresses the green one. 
set(op,'intercept',0.8);
set(op,'marker',subpop{2}.markers.Myosin.cytoplasm);
subpop{2}.markers.Actin.cytoplasm.AddOperation(op);
%Add Radial Gradient
op=Distance_To_Edge_Marker_Gradient();
set(op,'falloff_radius',40);
set(op,'falloff_type','Gaussian');
set(op,'increasing_or_decreasing','Increasing');
subpop{2}.markers.Actin.cytoplasm.AddOperation(op);
op=Perlin_Texture();
set(op,'amplitude',0.3);
set(op,'length_scale',5);
set(op,'frequency_falloff',0.8);
set(op,'noise_type','Turbulent');
subpop{2}.markers.Actin.cytoplasm.AddOperation(op)
% Turbulent Texture
op=Turbulent_Texture();
set(op,'max_displacement',5);
set(op,'length_scale',5);
set(op,'frequency_falloff',0.5);
subpop{2}.markers.Actin.cytoplasm.AddOperation(op);


%Marker 3
add_marker(subpop{2},'Density_Marker','Blue');
op=Cell_Density_Dependant_Marker_Level();
set(op,'max_level',2);
set(op,'falloff_radius',200);
set(op,'falloff_type','Gaussian');
set(op,'increasing_or_decreasing','Decreasing');
subpop{2}.markers.Density_Marker.nucleus.AddOperation(op);
%Perlin Texture
%Perlin Texture
op=Perlin_Texture();
set(op,'amplitude',0.7);
set(op,'length_scale',5);
set(op,'frequency_falloff',0.8);
% set(op,'noise_type','Turbulent');
subpop{2}.markers.Density_Marker.nucleus.AddOperation(op);


op=Constant_Dependant_Marker_Level();
set(op,'slope',-1.2);
set(op,'intercept',0.6);
set(op,'marker',subpop{2}.markers.Density_Marker.nucleus);
set(op,'region',subpop{2}.objects.nucleus);
subpop{2}.markers.Density_Marker.cytoplasm.AddOperation(op);
%Perlin Texture
op=Perlin_Texture();
set(op,'amplitude',0.4);
set(op,'length_scale',3);
set(op,'frequency_falloff',0.8);
set(op,'noise_type','Turbulent');
subpop{2}.markers.Density_Marker.cytoplasm.AddOperation(op);

%% SP2 Rendering
%Set the Cell Artifacts
op=Out_Of_Focus_Cells();
set(op,'fraction_blurred',1);
set(op,'blur_radius',2);
subpop{2}.add_cell_artifact(op);
op=Out_Of_Focus_Cells();
set(op,'fraction_blurred',0.1);
set(op,'blur_radius',4);
%subpop{2}.add_cell_artifact(op);

%% Common to all subpops

% %simucell_data.image_artif

% You can specify the fractions of cells in the image from the different subpopulations
simucell_data.population_fractions=[0.5 0.5]; % These must sum up to 1
% Note: subpopulation is chosen stochastically, and the above statement
% means each cell added has 50% probability of belonging to each
% subpopulation. Thus individual images will not have exactly this fraction
% of cells from the subpopulations

% The total number of cells in the image
simucell_data.number_of_cells=50;
% Note: If SimuCell cannot fit in any more cells (and still meet the
% overlap conditions) it will generate an image with the maximum number of
% cells it fits in

% The size of the image generated by SimuCell
simucell_data.simucell_image_size=[2000,2000];

% assign the subpopulations to the data structure
simucell_data.subpopulations=subpop;

