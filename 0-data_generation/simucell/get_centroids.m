function [centroids] = get_centroids(r)
m = length(r.mask_of_object_by_cell);
centroids = zeros(m, 3);
for i = 1:m
    img = r.mask_of_object_by_cell(i).nucleus;
    [y, x] = ndgrid(1:size(img, 1), 1:size(img, 2));
    centroid = mean([x(logical(img)), y(logical(img))]);
    centroids(i,:) = [centroid, r.subpopulation_numbers_of_cells(i)];
end

end

